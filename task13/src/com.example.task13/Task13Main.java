package com.example.task13;

public class Task13Main {
    public static void main(String[] args) {
        //здесь вы можете вручную протестировать ваше решение, вызывая реализуемый метод и смотря результат
        // например вот так:
        /*
        int[] arr = {9, 1100, 7, 8};
        removeMoreThen1000(arr);
        System.out.println(java.util.Arrays.toString(arr));
         */
    }

    static int[] removeMoreThen1000(int[] arr) {
        if (arr == null) return null;

        int[] newArr = new int[arr.length];
        int newIndex = 0;

        for (int j : arr) {
            if (j <= 1000) {
                newArr[newIndex] = j;
                newIndex++;
            }
        }

        int[] result = new int[newIndex];
        System.arraycopy(newArr, 0, result, 0, newIndex);
        return result;
    }

}