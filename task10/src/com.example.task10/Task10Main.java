package com.example.task10;

public class Task10Main {
    public static void main(String[] args) {
        //здесь вы можете вручную протестировать ваше решение, вызывая реализуемый метод и смотря результат
        // например вот так:
        /*
        int[] arr = {7, 5, 9};
        System.out.println(numMin(arr));
         */
    }

    static int numMin(int[] arr) {
        if (arr == null || arr.length == 0) return -1;
        int minVal = arr[0];
        int minIdx = 0;
        for(int idx=1; idx<arr.length; idx++) {
            if(arr[idx] <= minVal) {
                minVal = arr[idx];
                minIdx = idx;
            }
        }
        return minIdx;

    }

}