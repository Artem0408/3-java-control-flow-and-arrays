package com.example.task11;

public class Task11Main {
    public static void main(String[] args) {
        //здесь вы можете вручную протестировать ваше решение, вызывая реализуемый метод и смотря результат
        // например вот так:
        /*
        int[] arr = {7, 5, 9};
        swap(arr);
        System.out.println(java.util.Arrays.toString(arr));
         */
    }

    static void swap(int[] arr) {

        if (arr != null && arr.length != 0){
            int minVal = Integer.MAX_VALUE;
            int minIdx = 0;
            for(int idx=0; idx<arr.length; idx++) {
                if(arr[idx] <= minVal) {
                    minVal = arr[idx];
                    minIdx = idx;
                }
            }
            int swap = arr[0];
            arr[0] = arr[minIdx];
            arr[minIdx] = swap;
        }


    }

}